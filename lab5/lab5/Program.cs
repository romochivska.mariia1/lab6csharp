﻿using System;
using System.Data.SqlClient;

namespace lab5
{
    internal class Program
    {
        private static string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=sa2_romochivska;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False"; 

        private static void Main(string[] args)
        {
            Console.WriteLine("Лабораторна робота 5: Введіть номер запиту (1-14), або 0 для завершення:");

            while (true)
            {
                Console.Write("Введіть номер запиту: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    if (choice == 0)
                    {
                        Console.WriteLine("Програма завершена.");
                        break;
                    }

                    ExecuteQuery(choice);
                }
                else
                {
                    Console.WriteLine("Некоректний ввід. Введіть числове значення.");
                }
            }
        }

        private static void ExecuteQuery(int choice)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                switch (choice)
                {
                    case 1:
                        SimpleSelectQuery(connection);
                        break;
                    case 2:
                        Console.Write("Введіть частину предмету для пошуку: ");
                        string subjectInput = Console.ReadLine();
                        SpecialFunctionSelectQuery(connection, "LIKE", "Subject", $"%{subjectInput}%");
                        break;
                    case 3:
                        ComplexCriteriaSelectQuery(connection);
                        break;
                    case 4:
                        UniqueValuesSelectQuery(connection, "GroupName");
                        break;
                    case 5:
                        SimpleSelectQuery(connection, "Grade BETWEEN 80 AND 90");
                        break;
                    case 6:
                        SpecialFunctionSelectQuery(connection, ">", "Grade", "90 AND GroupName = 'GroupA'");
                        break;
                    case 7:
                        UniqueValuesSelectQuery(connection, "GroupName");
                        break;
                    case 8:
                        ComputedFieldSelectQuery(connection);
                        break;
                    case 9:
                        GroupingSelectQuery(connection, "GroupName");
                        break;
                    case 10:
                        SortingSelectQuery(connection, "LastName", "ASC");
                        break;
                    case 11:
                        SortingSelectQuery(connection, "LastName", "DESC");
                        break;
                    case 12:
                        ModificationQuery(connection, "Grade", "88", "LastName", "John Doe");
                        break;
                    case 13:
                        SpecialFunctionSelectQuery(connection, "=", "LastName", "John Doe");
                        break;
                    case 14:
                        // Додайте логіку для виконання 14-го запиту
                        break;
                    default:
                        Console.WriteLine("Некоректний вибір. Введіть номер від 1 до 14, або 0 для завершення.");
                        break;
                }
            }
        }

        private static void SimpleSelectQuery(SqlConnection connection, string additionalCondition = null)
        {
            string query = "SELECT * FROM Exams";
            if (!string.IsNullOrEmpty(additionalCondition))
            {
                query += " WHERE " + additionalCondition;
            }

            ExecuteSelectQuery(connection, query);
        }

        private static void SpecialFunctionSelectQuery(SqlConnection connection, string functionName, string columnName, string value)
        {
            string query = $"SELECT * FROM Exams WHERE {columnName} {functionName} @Value";
            SqlParameter parameter = new SqlParameter("@Value", value);
            ExecuteSelectQuery(connection, query, parameter);
        }

        private static void ComplexCriteriaSelectQuery(SqlConnection connection)
        {
            string query = "SELECT * FROM Exams WHERE Grade > @Grade AND GroupName = @GroupName";
            SqlParameter gradeParameter = new SqlParameter("@Grade", 90);
            SqlParameter groupParameter = new SqlParameter("@GroupName", "GroupA");
            ExecuteSelectQuery(connection, query, gradeParameter, groupParameter);
        }

        private static void UniqueValuesSelectQuery(SqlConnection connection, params string[] columnNames)
        {
            string columns = string.Join(", ", columnNames);
            string query = $"SELECT DISTINCT {columns} FROM Exams";
            ExecuteSelectQuery(connection, query);
        }

        private static void ComputedFieldSelectQuery(SqlConnection connection)
        {
            string query = "SELECT LastName, Grade, Grade * 2 AS DoubledGrade FROM Exams";
            ExecuteSelectQuery(connection, query);
        }

        private static void GroupingSelectQuery(SqlConnection connection, string groupByColumn)
        {
            string query = $"SELECT {groupByColumn}, AVG(Grade) AS AverageGrade FROM Exams GROUP BY {groupByColumn}";
            ExecuteSelectQuery(connection, query);
        }

        private static void SortingSelectQuery(SqlConnection connection, string orderByColumn, string sortOrder)
        {
            string query = $"SELECT * FROM Exams ORDER BY {orderByColumn} {sortOrder}";
            ExecuteSelectQuery(connection, query);
        }

        private static void ModificationQuery(SqlConnection connection, string columnName, string newValue, string conditionColumn, string conditionValue)
        {
            string query = $"UPDATE Exams SET {columnName} = @NewValue WHERE {conditionColumn} = @ConditionValue";
            SqlParameter newValueParameter = new SqlParameter("@NewValue", newValue);
            SqlParameter conditionValueParameter = new SqlParameter("@ConditionValue", conditionValue);
            ExecuteUpdateQuery(connection, query, newValueParameter, conditionValueParameter);
        }

        private static void ExecuteSelectQuery(SqlConnection connection, string query, params SqlParameter[] parameters)
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddRange(parameters);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Console.WriteLine("Результат запиту:");
                    while (reader.Read())
                    {
                        Console.WriteLine($"{reader["LastName"]}, {reader["GroupName"]}, {reader["Subject"]}, {reader["TicketNumber"]}, {reader["Grade"]}, {reader["Teacher"]}");
                    }
                    Console.WriteLine();
                }
            }
        }

        private static void ExecuteUpdateQuery(SqlConnection connection, string query, params SqlParameter[] parameters)
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddRange(parameters);

                int rowsAffected = command.ExecuteNonQuery();
                Console.WriteLine($"Кількість змінених рядків: {rowsAffected}");
            }
        }
    }
}
